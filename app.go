package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

type FileType string

type Config struct {
    ChatSettings map[int64]Language `json:"chat_settings,omitempty"`
}

type Language struct {
    Language string `json:"language"`
}



const FileVoice FileType = "voice"
const FileVoice FileType = "videomessage"

const configlocation = "./config/config.json"
const tokenlocation = "./config/token.txt"


func main() {
	config, err := readConfig(configlocation)
	if err != nil {
		log.Panicf("Config error: %v", err)
	}

	token, err := readBotToken(tokenlocation)
	if err != nil {
		log.Panicf("Token error: ", err)
	}
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)
	

	for update := range updates {

		if update.Message != nil {
			m := update.Message
			if m.From == nil {
				continue
			}

			log.Printf("Message received")
			err := handleMessage(bot, m, config, cf)
			if err != nil {
				log.Printf("[%s] %s,   err: %s", update.Message.From.UserName, update.Message.Text, err.Error())
				continue
			}

			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		}
	}
}
