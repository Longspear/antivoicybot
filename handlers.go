package main

import (
tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
"strings"
"os"
"log"
"fmt"
"github.com/fsnotify/fsnotify"
)


func CommandArguments(command string, message *tgbotapi.Message) string {
	return strings.TrimSpace(strings.TrimPrefix(message.Text, command))
}


func HandleLanguageCommand(bot *tgbotapi.BotAPI, message *tgbotapi.Message, config *Config) error {

	// Assuming the command format is "/language [language]"
	newLanguage := CommandArguments["/language", message]
	existingLanguage, exists := config.ChatSettings[message.Chat.ID]

	if !exists || (exists && existingLanguage.Language != newLanguage) {
		config.ChatSettings[message.Chat.ID] = Language{Language: newLanguage}
		err = WriteConfig(&config)
		if err != nil {
			return err
		}
	}

	return nil
}


//                    err := os.Remove(myResponse.FileName)
  //                  if err != nil {
   //                     log.Printf("Error deleting media: %v", err)
 









func TranscribeAudio(filePath, language string) (string, error) {
	ctx := context.Background()
	
	client, err := speech.NewClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()
	
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}
	
	var encoding speechpb.RecognitionConfig_AudioEncoding
	fileExt := getFileExtension(filePath)
		
	switch fileExt {
	case ".ogg":
		encoding = speechpb.RecognitionConfig_OGG_OPUS
	case ".mp4":
		encoding = speechpb.RecognitionConfig_LINEAR16
	default:
		return "", fmt.Errorf("unsupported file format: %s", fileExt)
		}
		
	req := &speechpb.RecognizeRequest{
		Config: &speechpb.RecognitionConfig{
			Encoding:        encoding,
			SampleRateHertz: 16000, // Adjust the sample rate according to your audio file
			LanguageCode:    language,
		},
		Audio: &speechpb.RecognitionAudio{
			AudioSource: &speechpb.RecognitionAudio_Content{Content: data},
		},
	}
		
	resp, err := client.Recognize(ctx, req)
	if err != nil {
		return "", err
	}
		
	for _, result := range resp.Results {
		for _, alt := range result.Alternatives {
			return alt.Transcript, nil
		}
	}
		
	return "", fmt.Errorf("no transcription found")
}
		
func getFileExtension(filePath string) string {
	return filePath[strings.LastIndex(filePath, "."):]
}



type commandHandlerFunc func(*tgbotapi.BotAPI, *tgbotapi.Message, *Config, ConfigWriter) error

func handleMessage(bot *tgbotapi.BotAPI, message *tgbotapi.Message, config *Config, configwriter ConfigWriter) error {
	

	
	receivedMessage := message.Text
	ChatID = message.Chat.ID
	
	commandHandlers := map[string]commandHandlerFunc{
		"language":    HandleLanguageCommand,
	}

	command := message.Command()
	if handler, ok := commandHandlers[command]; ok {
		return handler(bot, message, config)
	}

	if message.Voice != nil { // voice proccess
		voiceFileID := message.Voice.FileID
		fileName, err := downloadAndSaveFile(bot, voiceFileID, "./voices", ".ogg")
	} else if message.VideoNote != nil { // video message proccess
    	videoNoteFileID := message.VideoNote.FileID
    	fileName, err := downloadAndSaveFile(bot, videoNoteFileID, "./videonotes", ".mp4")
	} else {
		return nil
	}

	chatlanguage, err := GetLanguageForChatID(config, message.Chat.ID)

	if err != nil {
		fmt.Errorf(err)
		return err
	}
	
	speechtotext, err := TranscribeAudio (fileName, config.[message.Chat.ID])
	if err != nil {
		fmt.Errorf(err)
		return err
	} else {
		msg := msg := tgbotapi.NewMessage(message.Chat.ID, speechtotext)
		bot.Send(msg)
	}

}

func GetLanguageForChatID(config *Config, chatID int64) (string, error) {
    language, ok := config.ChatSettings[chatID]
    if !ok {
        return "", fmt.Errorf("chat ID %d not found in config", chatID)
    }
    if len(language) == 0 {
        return "", fmt.Errorf("no language specified for chat ID %d", chatID)
    }
    return language[0], nil
}